<?php

return [
    'Pid'  =>  '父ID',
    'Fullname'  =>  '部门全称',
    'Description'  =>  '描述',
    'Createtime'  =>  '创建时间',
    'Updatetime'  =>  '修改时间',
    'Weigh'  =>  '权重',
    'Status'  =>  '状态'
];
