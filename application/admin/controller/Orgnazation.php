<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use app\admin\model\Orgnazation as OrgnazationModel;
use fast\Tree;

/**
 * 组织管理
 *
 * @icon fa fa-circle-o
 */
class Orgnazation extends Backend
{
    
    /**
     * Orgnazation模型对象
     */
    protected $model = null;
	protected $orglist = [];
	protected $noNeedRight = ['selectpage'];

    public function _initialize()
    {
        parent::_initialize();
		$this->request->filter(['strip_tags']);
        $this->model = model('Orgnazation');
		
		$tree = Tree::instance();
        $tree->init(collection($this->model->order('weigh desc,id desc')->select())->toArray(), 'pid');
        $this->orglist = $tree->getTreeList($tree->getTreeArray(0), 'name');
		//halt($this->orglist);
        $orgdata = [0 => ['type' => 'all', 'name' => __('None')]];
        foreach ($this->orglist as $k => $v)
        {
            $orgdata[$v['id']] = $v;
        }
        //$this->view->assign("flagList", $this->model->getFlagList());
        //$this->view->assign("typeList", OrgnazationModel::getTypeList());
        $this->view->assign("parentList", $orgdata);

    }
    
    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    

}
